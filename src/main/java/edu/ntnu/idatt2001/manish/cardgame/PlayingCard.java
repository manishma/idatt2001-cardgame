package edu.ntnu.idatt2001.manish.cardgame;

import javafx.scene.image.Image;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class PlayingCard {

    private final char suit; // 'S'=spade, 'H'=heart, 'D'=diamonds, 'C'=clubs
    private final int face; // a number between 1 and 13

    /**
     * Creates an instance of a PlayingCard with a given suit and face.
     *
     * @param suit The suit of the card, as a single character. 'S' for Spades,
     *             'H' for Heart, 'D' for Diamonds and 'C' for clubs
     * @param face The face value of the card, an integer between 1 and 13
     */
    public PlayingCard(char suit, int face) {
        this.suit = suit;
        this.face = face;
    }

    /**
     * Returns the suit and face of the card as a string.
     * A 4 of hearts is returned as the string "H4".
     *
     * @return the suit and face of the card as a string
     */
    public String getAsString() {
        return String.format("%s%s", suit, face);
    }

    /**
     * Returns the suit of the card, 'S' for Spades, 'H' for Heart, 'D' for Diamonds and 'C' for Clubs
     *
     * @return the suit of the card
     */
    public char getSuit() {
        return suit;
    }

    /**
     * Returns the face of the card (value between 1 and 13).
     *
     * @return the face of the card
     */
    public int getFace() {
        return face;
    }

    public Image getFaceImage(){
        String currentSuit = "";
        if (getSuit() == 'S') {
            currentSuit = "spades";
        }else if (getSuit() == 'H'){
            currentSuit = "hearts";
        }else if (getSuit() == 'D'){
            currentSuit = "diamonds";
        }else if (getSuit() == 'C'){
            currentSuit = "clubs";
        }
        //return new Image(getClass().getResourceAsStream("src/main/resources/cardsPNG/"+getFace()+"_of_"+currentSuit+".png"));
        FileInputStream imgStream = null;
        try {
            imgStream = new FileInputStream("src/main/resources/cardsPNG/"+getFace()+"_of_"+currentSuit+".png");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return new Image(imgStream);
    }

    @Override
    public String toString() {
        return "PlayingCard{" +
                "suit=" + suit +
                ", face=" + face +
                '}';
    }
}
