package edu.ntnu.idatt2001.manish.cardgame;
import javafx.application.Application;
import javafx.scene.*;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.fxml.FXMLLoader;
import java.io.IOException;

public class GUI extends Application {
    
    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(GUI.class.getResource("layout.fxml"));
        stage.setTitle("Cardgame");
        stage.setScene(new Scene(fxmlLoader.load(), 820, 620));
        stage.show();

        Label title = (Label) stage.getScene().lookup("#title");
        stage.widthProperty().addListener((obs, oldVal, newVal) -> {
            title.setStyle("-fx-font-size:" + stage.widthProperty().getValue()/20 + ";");
        });

        stage.heightProperty().addListener((obs, oldVal, newVal) -> {
            title.setStyle("-fx-font-size:" + stage.heightProperty().getValue()/20 + ";");
        });
    }
}