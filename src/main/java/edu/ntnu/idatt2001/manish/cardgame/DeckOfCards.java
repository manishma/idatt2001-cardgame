package edu.ntnu.idatt2001.manish.cardgame;
import java.util.ArrayList;
import java.util.Random;

public class DeckOfCards {
    private final char[] suits = { 'S', 'H', 'D', 'C' };
    private ArrayList<PlayingCard> deck = new ArrayList<>();

    public DeckOfCards(){
        for (int i = 0; i < suits.length; i++) {
            for (int j = 1; j <= 13; j++) {
                deck.add(new PlayingCard(suits[i],j));
            }
        }

        if(deck.size() != 52) {throw new IllegalArgumentException("Ya deck aint big enough");}
    }

    public ArrayList<PlayingCard> getDeck() {
        return this.deck;
    }

    public void setDeck(ArrayList<PlayingCard> deck) {
        this.deck = deck;
    }

    public ArrayList<PlayingCard> dealHand(int i){
        Random random = new Random();
        ArrayList<PlayingCard> randomCards = new ArrayList<>();
        for (int j = 0; j < i; j++) {
            randomCards.add(deck.get(random.nextInt(52)));
        }
        return randomCards;
    }

    public int sumOfHand(ArrayList<PlayingCard> cards){
        int sum = 0;
        for(PlayingCard card : cards){
            sum += card.getFace();
        }
        return sum;
    }

    public String heartAmount(ArrayList<PlayingCard> cards){
        String hearts = "";
        for(PlayingCard card : cards){
            if (card.getSuit() == 'H') {
                hearts += "H" + card.getFace() + " ";                
            }
        }
        if (hearts.length() == 0) {
            return "No Hearts";
        }
        return hearts;
    }

    public boolean flush(ArrayList<PlayingCard> cards){
        boolean flush = true;
        for (int i = 0; i < cards.size(); i++) {
            char suit = cards.get(0).getSuit();
            if(cards.get(i).getSuit() != suit){
                flush = false;
            }
        }
        if (flush && cards.size() != 0){
            return true;
        }
        else{
            return false;
        }
    }

    public boolean spadeQueen(ArrayList<PlayingCard> cards){
        for (PlayingCard card : cards) {
            if (card.getFace() == 12 && card.getSuit() == 'S') {
                return true;
            }
        }
        return false;
    }
}
