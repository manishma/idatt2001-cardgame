package edu.ntnu.idatt2001.manish.cardgame;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;

import java.util.ArrayList;

public class HelloController {
    DeckOfCards deck = new DeckOfCards();
    private ArrayList<PlayingCard> cards = new ArrayList<>();

    @FXML
    private Button dealHandButton;
    @FXML
    private Button checkHandButton;
    @FXML
    private TextField sumOfFaces;
    @FXML
    private TextField cardsOfHearts;
    @FXML
    private TextField flush;
    @FXML
    private TextField queenOfSpades;

    @FXML
    private ImageView img1;
    @FXML
    private ImageView img2;
    @FXML
    private ImageView img3;
    @FXML
    private ImageView img4;
    @FXML
    private ImageView img5;


    @FXML
    private void dealHandAction(ActionEvent event) {
        cards.clear();
        cards = deck.dealHand(5);
        sumOfFaces.setText("");
        cardsOfHearts.setText("");
        flush.setText("");
        flush.setStyle("-fx-border-color: inherit;");
        queenOfSpades.setText("");
        queenOfSpades.setStyle("-fx-border-color: inherit;");
        fillImages();
    }

    private void fillImages() {
        img1.setImage(cards.get(0).getFaceImage());
        img2.setImage(cards.get(1).getFaceImage());
        img3.setImage(cards.get(2).getFaceImage());
        img4.setImage(cards.get(3).getFaceImage());
        img5.setImage(cards.get(4).getFaceImage());
    }

    @FXML
    private void checkHandAction() {
        sumOfFaces.setText(String.valueOf(deck.sumOfHand(cards)));
        cardsOfHearts.setText(deck.heartAmount(cards));
        boolean flushState = deck.flush(cards);
        flush.setText(flushState ? "Yes" : "No");
        flush.setStyle(flushState ? "-fx-background-color: #21e065;": "-fx-background-color: #e02121;");
        boolean queenOfSpadesStatus = deck.spadeQueen(cards);
        queenOfSpades.setText(queenOfSpadesStatus ? "Yes" : "No");
        queenOfSpades.setStyle(queenOfSpadesStatus ? "-fx-background-color: #21e065;" : "-fx-background-color: #e02121;");
    }
}