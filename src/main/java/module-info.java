module edu.ntnu.idatt2001.manish.cardgame {
    requires javafx.controls;
    requires javafx.fxml;


    opens edu.ntnu.idatt2001.manish.cardgame to javafx.fxml;
    exports edu.ntnu.idatt2001.manish.cardgame;
}